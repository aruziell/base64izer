package aru.base64izer;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.Arrays;
import java.util.Base64;
import java.util.Base64.Decoder;
import java.util.Base64.Encoder;
import java.util.Iterator;

import javafx.application.Platform;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.ObservableList;
import javafx.concurrent.Service;
import javafx.concurrent.Task;


public class Base64Service
extends Service<Void>
{
	ObservableList<File> fileList;
	StringProperty outDirProperty = new SimpleStringProperty();
	StringProperty speedProperty = new SimpleStringProperty( this, "speedProperty" );

	public Base64Service( ObservableList<File> fileList )
	{
		super();
		this.fileList = fileList;
	}

	public StringProperty getOutDirProperty()
	{
		return outDirProperty;
	}

	public StringProperty getSpeedProperty()
	{
		return speedProperty;
	}

	@Override
	protected Task<Void> createTask()
	{
		Task<Void> t = new Task<Void>()
		{
			@Override
			protected Void call()
			throws Exception
			{
				updateProgress( 0, 0 );

				Iterator<File> it = fileList.iterator();

				double currentTime = System.currentTimeMillis()/1000.0,
					dt, speed = 0.0;

				while( it.hasNext() )
				{
					File f = it.next();

					// lets check if we'll be encoding or decoding
					boolean encoding = f.getName().endsWith( ".base64" ) ? false : true;

					FileInputStream fis = new FileInputStream( f );

					StringBuffer outFilePath
					= new StringBuffer( outDirProperty.get()
						+ File.separator + f.getName() );

					// decide about a name of a new file
					if( encoding )
						outFilePath.append( ".base64" );
					else
						outFilePath.delete( outFilePath.length()-7, outFilePath.length() );

					File outFile = new File( outFilePath.toString() );
					FileOutputStream fos = new FileOutputStream( outFile );

					try
					{
						Encoder encoder = Base64.getEncoder();
						Decoder decoder = Base64.getDecoder();

						byte[] buffer = new byte[3 * 1024 * 1024], dest;

						if( encoding )
							System.out.print( "Encoding to " );
						else
							System.out.print( "Decoding to " );
						System.out.println( outFile.getAbsolutePath() );

						int n, bytesRead = 0;
						long fileLength = f.length();
						while( ( n = fis.read( buffer ) ) != -1 )
						{
							// don't let coding more than necessary
							dest = Arrays.copyOf( buffer, n );

							if( encoding )
								dest = encoder.encode( dest );
							else
								dest = decoder.decode( dest );

							fos.write( dest );
							
							bytesRead += n;
							updateProgress( bytesRead, fileLength );

							// delta time passed since last update in seconds
							dt = System.currentTimeMillis() / 1000.0 - currentTime;
							currentTime += dt;
							
							if( dt <= 0 )
								dt = Double.MIN_VALUE;

// this defines how relevant is last speed value in next approximation
							double factor = .05;
							speed = speed * ( 1.0 - factor )
								+ ( n / dt ) * factor / ( 1024 * 1024 );
//							speed = n / dt;

							final String speedString =
								( Math.round( 100 * speed ) / 100.0 )
								+ " MiBi / s";

							Platform.runLater( new Runnable()
							{
								@Override
								public void run()
								{
									speedProperty.set( speedString );
								}
							});
						}

						System.out.println( "File done" );
					}
					catch( Exception e )
					{
						e.printStackTrace();
					}
					finally
					{
						fis.close();
						fos.close();
					}
				}
				updateProgress( 1, 0 );
				System.out.println( "Task done" );
				return null;
			}
		};
		return t;
	}
}
