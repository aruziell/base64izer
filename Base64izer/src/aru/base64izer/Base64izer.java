package aru.base64izer;

import java.io.File;
import java.util.List;

import javafx.application.Application;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TextField;
import javafx.scene.input.DragEvent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javafx.stage.Stage;


public final class Base64izer
extends Application
{
	GridPane root = new GridPane();

	ObservableList<File> fileList = FXCollections.observableArrayList();
	ListView<File> listView = new ListView<File>( fileList );

	Button addFileButton = new Button( "add.." );
	Button removeFileButton = new Button( "remove" );

	ProgressBar progressBar = new ProgressBar();

	TextField outputDirField = new TextField( System.getProperty( "user.home" ) );
	Button outputDirButton = new Button( "select directory.." );

	Button startButton = new Button( "encode/decode" );

	StringProperty copyingSpeed = new SimpleStringProperty();
	Label speedLabel = new Label();

	// Coder service
	Base64Service base64Service = new Base64Service( fileList );

	@Override
	public void init()
	throws Exception
	{
		initGUI();
		initGUIControlls();
		initProperties();
//		root.setGridLinesVisible( true );
	}

	private void initGUI()
	{
		root.setAlignment( Pos.CENTER );
		root.setHgap( 8 );
		root.setVgap( 8 );
		root.setPadding( new Insets( 24, 24, 24, 24 ) );

		int y = 0;

		listView.setPrefSize( 480, 180 );
		listView.getSelectionModel().setSelectionMode( SelectionMode.MULTIPLE );
		root.add( listView, 1, y++ );

		HBox hBox = new HBox( 8 );
		hBox.setAlignment( Pos.CENTER_RIGHT );
		hBox.getChildren().addAll( addFileButton, removeFileButton );
		root.add( hBox, 1, y++ );

		outputDirField.setEditable( false );
		root.add( outputDirField, 1, y++ );

		hBox = new HBox( 8 );
		hBox.setAlignment( Pos.CENTER_RIGHT );
		hBox.getChildren().addAll( outputDirButton, startButton );
		root.add( hBox, 1, y++ );

		progressBar.setProgress( 0 );
		progressBar.setPrefWidth( listView.getPrefWidth() );
		root.add( progressBar, 1, y++ );

		root.add( speedLabel, 1, y++ );
	}

	private void initGUIControlls()
	{
		listView.setOnDragOver( new EventHandler<DragEvent>()
		{
			@Override
			public void handle( DragEvent event )
			{
				Dragboard db = event.getDragboard();

				if( db.hasFiles() )
					event.acceptTransferModes( TransferMode.COPY );
				else
					event.consume();
			}
		} );

		listView.setOnDragDropped( new EventHandler<DragEvent>()
		{
			@Override
			public void handle( DragEvent event )
			{
				Dragboard db = event.getDragboard();

				if( db.hasFiles() )
				{
					for( File f : db.getFiles() )
					{
						if( f.isFile() && !fileList.contains( f ) )
							fileList.add( f );
					}
				}

				event.setDropCompleted( true );
				event.consume();
			}
		} );

		addFileButton.addEventHandler( ActionEvent.ACTION, new EventHandler<ActionEvent>()
		{
			@Override
			public void handle( ActionEvent event )
			{
				FileChooser fc = new FileChooser();
				fc.setTitle( "Choose file.." );
				List<File> files = fc.showOpenMultipleDialog( null );
				if( files == null )
					return;
				
				for( File f : files )
					if( !fileList.contains( f ) )
						fileList.add( f );
			}
		} );

		removeFileButton.addEventHandler( ActionEvent.ACTION, new EventHandler<ActionEvent>()
		{
			@Override
			public void handle( ActionEvent event )
			{
				List<File> selectedFiles = listView.getSelectionModel().getSelectedItems();
				fileList.removeAll( selectedFiles );
			}
		});

		outputDirButton.addEventHandler( ActionEvent.ACTION, new EventHandler<ActionEvent>()
		{
			@Override
			public void handle( ActionEvent event )
			{
				DirectoryChooser dc = new DirectoryChooser();
				dc.setTitle( "Select output path.." );
				File f = dc.showDialog( null );
				if( f != null && f.isDirectory() )
					outputDirField.setText( f.getAbsolutePath() );
			}
		} );

		startButton.setOnAction( new EventHandler<ActionEvent>()
		{
			@Override
			public void handle( ActionEvent event )
			{
				if( !base64Service.isRunning() )
				{
					base64Service.restart();
				}
			}
		} );
	}

	private void initProperties()
	{
		progressBar.progressProperty().bind( base64Service.progressProperty() );

		addFileButton.disableProperty().bind( base64Service.runningProperty() );
		removeFileButton.disableProperty().bind( base64Service.runningProperty() );
		progressBar.visibleProperty().bind( base64Service.runningProperty() );
		startButton.disableProperty().bind( base64Service.runningProperty() );
		outputDirButton.disableProperty().bind( base64Service.runningProperty() );

		base64Service.getOutDirProperty().bind( outputDirField.textProperty() );

		speedLabel.textProperty().bind( base64Service.getSpeedProperty() );
		speedLabel.visibleProperty().bind( base64Service.runningProperty() );
	}

	@Override
	public void start( Stage primaryStage )
	throws Exception
	{
		primaryStage.setTitle( getClass().getSimpleName() );
		Scene scene = new Scene( root );
		primaryStage.setScene( scene );
		primaryStage.show();
	}

	public static void main( String[] args )
	{
		launch( args );
	}
}
